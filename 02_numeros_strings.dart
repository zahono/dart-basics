void main() {
//   Números
  int empleados = 5;
  double pi = 3.14159;
  var numero = 1.0;

  print('$empleados - $pi - $numero');

//   String - Cadena de caracteres
  String nombre = 'Santiago';
  print(nombre);
  print(nombre[0]);
  print(nombre[nombre.length - 1]);
}
