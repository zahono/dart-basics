/* import 'dart:io';

Future main() async {
  var server = await HttpServer.bind(
    InternetAddress.loopbackIPv4,
    4040,
  );
  print('Listening on localhost:${server.port}');

  await for (HttpRequest request in server) {
    request.response.write('Hello, world!');
    await request.response.close();
  }
} */

void main() async {
  print('Estamos a punto de pedir datos');

  /* httpGet('https://api.nasa.us/aliens')
      .then((value) => print(value))
      .catchError((onError) => print(onError)); */

  /* Async - await */
  String data = await httpGet('https://api.nasa.us/aliens');
  print(data);

  print('Ultima linea');
}

Future<String> httpGet(String url) {
  return Future.delayed(new Duration(seconds: 4), () {
    return 'Oelo, Panguana';
  });
}
