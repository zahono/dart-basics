void main() {
  final cuadrado = new Cuadrado();

  cuadrado.lado = 10;

  print(cuadrado);
  print('área: ${cuadrado.area}');
}

class Cuadrado {
  double _lado;
  // double _area;

  set lado(double value) => {
        if (value <= 0)
          {throw ('El lado no puede ser menor a 0')}
        else
          {_lado = value}
      };

  double get area => _lado * _lado;

  toString() => 'Lado: $_lado';
}
