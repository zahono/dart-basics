import 'dart:convert';

void main() {
  // final wolverine = new Heroe('Logan', 'Regeneración y capacidades sobrehumanas');

  final rawJson =
      '{ "nombre": "Logan", "poder": "Regeneración y capacidades sobrehumanas" }';

  Map parsedJson = json.decode(rawJson);

  // print(parsedJson);

  final wolverine = new Heroe.fromJson(parsedJson);

  print(wolverine.nombre);
  print(wolverine.poder);
}

class Heroe {
  String nombre;
  String poder;

  Heroe(this.nombre, this.poder);

  Heroe.fromJson(Map parsedJson) {
    nombre = parsedJson['nombre'];
    poder = parsedJson['poder'];
  }

  String toString() => 'nombre: $nombre - poder: $poder';
}
