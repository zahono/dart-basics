void main() {
  final wolverine = new Heroe(
      poder: 'Regeneración y capacidades sobrehumanas', nombre: 'Logan');
  print(wolverine);
  // print(wolverine.poder);
  // print(wolverine.nombre);
  // print(wolverine.toString());
}

class Heroe {
  String nombre;
  String poder;

  //El constructor tiene el mismo nombre de la clase
  /* Heroe({String nombre = 'Sin Nombre', String poder}) {
    this.nombre = nombre;
    this.poder = poder;
  } */
  /* Minimizando codigo en el constructor */
  Heroe({this.nombre, this.poder});

  /* String toString() {
    // return 'nombre: ${this.nombre} - poder: ${this.poder}';
    return 'nombre: $nombre - poder: $poder';
  } */
  /* Minimizando codigo en el metodo */
  String toString() => 'nombre: $nombre - poder: $poder';
}
