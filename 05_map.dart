void main() {
  String propiedad = 'soltero';
  Map<String, dynamic> persona = {
    'nombre': 'Carlos',
    'edad': 32,
    'soltero': true
  };
  print('La persona se llama ${persona['nombre']}');
  print('${persona['nombre']} tiene ${persona['edad']} años');
  print(
      '${persona['nombre']} está ${(persona['soltero'] ? 'soltero' : 'casado')}');
  print(persona[propiedad]);

  Map<int, String> personas = {1: 'Tony', 2: 'Peter', 9: 'Strange'};
  print(personas);

  personas.addAll({4: 'Bruce'});
  print(personas);
  print(personas[2]);
}
